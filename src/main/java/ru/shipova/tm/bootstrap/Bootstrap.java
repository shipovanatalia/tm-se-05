package ru.shipova.tm.bootstrap;

import ru.shipova.tm.api.ServiceLocator;
import ru.shipova.tm.command.*;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.TerminalService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс загрузчика приложения
 */

public class Bootstrap implements ServiceLocator {

    private TaskRepository taskRepository = new TaskRepository();
    private ProjectRepository projectRepository = new ProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private TaskService taskService = new TaskService(taskRepository, projectRepository);
    private TerminalService terminalService = new TerminalService(this);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();


    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String commandName = command.getName(); //Command Line Interface
        final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    private void execute(final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws Exception {
        loadCommands();

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            execute(commandName);
        }
    }

    private void loadCommands() {
        try {
            registry(new HelpCommand());
            registry(new ExitCommand());
            registry(new ProjectListCommand());
            registry(new ProjectCreateCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectRemoveCommand());
            registry(new TaskClearCommand());
            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskRemoveCommand());
            registry(new TaskShowCommand());
        } catch (CommandCorruptException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }
}
