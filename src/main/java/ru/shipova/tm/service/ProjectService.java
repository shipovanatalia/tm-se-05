package ru.shipova.tm.service;

import ru.shipova.tm.entity.Project;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void create(String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        String projectId = UUID.randomUUID().toString();
        projectRepository.persist(new Project(projectId, projectName));
    }

    public List<Project> getListProject() {
        return projectRepository.findAll();
    }

    public void clear() {
        projectRepository.removeAll();
    }

    public void remove(String projectName) {
        if (projectName == null || projectName.isEmpty()) return;

        String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) return;

        projectRepository.remove(projectId);
        taskRepository.removeAllTasksOfProject(projectId);
    }
}
