package ru.shipova.tm.service;

import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.command.AbstractCommand;

import java.util.List;
import java.util.Scanner;

public class TerminalService {
    private Bootstrap bootstrap;
    private Scanner in = new Scanner(System.in);

    public TerminalService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public List<AbstractCommand> getCommands(){
        return bootstrap.getCommands();
    }

    public String nextLine(){
        return in.nextLine();
    }
}
