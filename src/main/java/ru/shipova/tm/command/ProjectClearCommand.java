package ru.shipova.tm.command;

import ru.shipova.tm.service.ProjectService;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
