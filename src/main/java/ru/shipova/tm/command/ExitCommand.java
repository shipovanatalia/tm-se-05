package ru.shipova.tm.command;

public class ExitCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute(){
        System.exit(0);
    }
}
