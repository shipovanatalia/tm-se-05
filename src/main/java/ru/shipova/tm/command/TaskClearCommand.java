package ru.shipova.tm.command;

import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        TaskService taskService = serviceLocator.getTaskService();
        taskService.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }
}
