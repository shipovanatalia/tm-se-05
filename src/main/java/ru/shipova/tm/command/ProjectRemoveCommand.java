package ru.shipova.tm.command;

import ru.shipova.tm.service.ProjectService;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME:");
        ProjectService projectService = serviceLocator.getProjectService();
        String projectName = serviceLocator.getTerminalService().nextLine();
        projectService.remove(projectName);
        System.out.println("[PROJECT " + projectName + " REMOVED]");
    }
}
