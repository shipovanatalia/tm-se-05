package ru.shipova.tm.command;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute(){
        for (final AbstractCommand command :
                serviceLocator.getTerminalService().getCommands()) {
            System.out.println(command.getName() + ": "
                    + command.getDescription());
        }
    }
}
