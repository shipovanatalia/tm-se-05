package ru.shipova.tm.command;

import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.service.TaskService;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        TaskService taskService = serviceLocator.getTaskService();
        String taskName = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER PROJECT NAME:");
        String projectName = serviceLocator.getTerminalService().nextLine();
        try {
            taskService.create(taskName, projectName);
        } catch (ProjectDoesNotExistException e) {
            System.out.println("PROJECT DOES NOT EXISTS");
        }
        System.out.println("[OK]");
    }
}
