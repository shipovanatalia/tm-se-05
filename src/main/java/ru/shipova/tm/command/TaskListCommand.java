package ru.shipova.tm.command;

import ru.shipova.tm.entity.Task;
import ru.shipova.tm.service.TaskService;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        TaskService taskService = serviceLocator.getTaskService();

        int index = 1;
        for (Task task : taskService.getListTask()) {
            System.out.println(index++ + ". " + task.getName());
        }
        System.out.println();
    }
}
