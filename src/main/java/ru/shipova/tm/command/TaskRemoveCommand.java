package ru.shipova.tm.command;

import ru.shipova.tm.service.TaskService;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME:");
        TaskService taskService = serviceLocator.getTaskService();
        String projectName = serviceLocator.getTerminalService().nextLine();
        taskService.remove(projectName);
        System.out.println("[TASK " + projectName + " REMOVED]");
    }
}
