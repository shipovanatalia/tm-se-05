package ru.shipova.tm.command;

import ru.shipova.tm.entity.Project;
import ru.shipova.tm.service.ProjectService;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        ProjectService projectService = serviceLocator.getProjectService();

        int index = 1;
        for (Project project : projectService.getListProject()) {
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println();
    }
}
