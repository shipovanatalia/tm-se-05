package ru.shipova.tm.command;

import ru.shipova.tm.service.ProjectService;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        ProjectService projectService = serviceLocator.getProjectService();
        String projectName = serviceLocator.getTerminalService().nextLine();
        projectService.create(projectName);
        System.out.println("[OK]");
    }
}
